package com.drivetogether.drivetogether;

import com.skobbler.ngx.SKCoordinate;
import com.skobbler.ngx.map.SKPolyline;

/**
 * Created by Antek on 2017-01-23.
 */

public interface ServerListener {
    void setUserPosition(String userID, SKCoordinate jsonCoordinate);
    void setUserRoute(String userID, SKPolyline jsonCoordinate);
    void removeUser(String userID);
}
