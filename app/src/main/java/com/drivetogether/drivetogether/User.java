package com.drivetogether.drivetogether;

import com.skobbler.ngx.SKCoordinate;
import com.skobbler.ngx.map.SKAnimationSettings;
import com.skobbler.ngx.map.SKAnnotation;
import com.skobbler.ngx.map.SKMapSurfaceView;
import com.skobbler.ngx.map.SKPolyline;


public class User {

    public User(int id, SKMapSurfaceView mapView){
        this.id=id+100;//Początkowe id-ki mogą być zajęte przez mapę
        this.mapView = mapView;
    }

    public void setPosition(SKCoordinate coordinate) {
        if(position == null)
        {
            SKAnnotation position = new SKAnnotation(id);
            position.setAnnotationType(SKAnnotation.SK_ANNOTATION_TYPE_GREEN);
            position.setLocation(coordinate);
            mapView.addAnnotation(position, SKAnimationSettings.ANIMATION_NONE);
        }
        else {
            position.setLocation(coordinate);
            mapView.updateAnnotation(position);
        }
    }

    public void setRoute(final SKPolyline route) {
        route.setColor(new float[]{ 0f, 0f, 1f, 1f});
        route.setIdentifier(id+100);
        mapView.addPolyline(route);
    }

    public void removeUser()
    {
        SKPolyline route = new SKPolyline();
        route.setColor(new float[]{ 0f, 0f, 0f, 0f});
        route.setIdentifier(id+100);
        mapView.addPolyline(route);
        mapView.clearOverlay(id+100);
        mapView.deleteAnnotation(id);
    }

    private SKMapSurfaceView mapView;
    private SKAnnotation position;
    private int id;
}
