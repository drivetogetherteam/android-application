package com.drivetogether.drivetogether;

/**
 * Created by gumny on 28.01.17.
 */

public interface ServerConnector {
    void connect();
    void send(String text);
    <Payload> void send(Payload coordinate);
}
