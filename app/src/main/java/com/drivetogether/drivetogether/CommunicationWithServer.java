package com.drivetogether.drivetogether;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.skobbler.ngx.SKCoordinate;
import com.skobbler.ngx.map.SKPolyline;

import org.java_websocket.WebSocketImpl;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ServerHandshake;

import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;

public class CommunicationWithServer implements ServerConnector{
    private String TAG = "CommunicationWithServer";
    private WebSocketClient mWebSocketClient;
    private Gson gson = new Gson();
    private boolean isConnected = false;
    private ServerListener listener;
    private String id = "";
    public CommunicationWithServer(ServerListener listener){
        this.listener = listener;
    }

    @Override
    public void connect() {
        URI uri;
        try {
            uri = new URI("ws://172.16.79.145:8080/DriveTogether/drive");
//            uri = new URI("ws://89.70.241.214:8643/DriveTogether/drive");
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }
        WebSocketImpl.RCVBUF = 10000000;
        mWebSocketClient = new WebSocketClient(uri, new Draft_17()) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                isConnected = true;
                Log.i(TAG, "Opened");
            }

            @Override
            public void onMessage(String s) {
                Log.i(TAG, "Message recived: " + s);
                if(id.isEmpty())
                    id = s;
                else
                {
                    JsonObject json = new JsonParser().parse(s).getAsJsonObject();
                    switch (json.get("type").getAsString()){
                        case "SKCoordinate":
                            Type coordinateType = new TypeToken<MessageWrapper<SKCoordinate>>() {}.getType();
                            MessageWrapper<SKCoordinate> coordinate = gson.fromJson(s, coordinateType);
                            Log.d(TAG, "onMessage: id = " + coordinate.id + "SKCoordinate = " + coordinate.payload);
                            listener.setUserPosition(coordinate.id, coordinate.payload);
                            break;
                        case "SKPolyline":
                            Type routeType = new TypeToken<MessageWrapper<SKPolyline>>() {}.getType();
                            MessageWrapper<SKPolyline> route = gson.fromJson(s, routeType);
                            Log.d(TAG, "onMessage: id = " + route.id + "route = " + route.payload.toString());
                            listener.setUserRoute(route.id, route.payload);
                            break;
                        case "Disconnect":
                            listener.removeUser(json.get("id").getAsString());
                            break;
                    }
                }
            }

            @Override
            public void onClose(int i, String s, boolean b) {
                isConnected = false;
                Log.i(TAG, "Closed " + s);
            }

            @Override
            public void onError(Exception e) {
                Log.i(TAG, "Error " + e.getMessage());
            }
        };
        mWebSocketClient.connect();
    }

    @Override
    public void send(String text) {
        if (!id.isEmpty() && isConnected) {
            Log.i(TAG, "send: " + text);
            mWebSocketClient.send(text);
        }
    }

    @Override
    public <Payload> void send(Payload coordinate)
    {
        send(
                gson.toJson(
                        new MessageWrapper<>(coordinate, id)
                )
        );
    }
}
