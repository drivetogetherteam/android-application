package com.drivetogether.drivetogether;

/**
 * Created by gumny on 28.01.17.
 */

public class MessageWrapper<Payload> {
    public String type;
    public String id;
    public Payload payload;

    @SuppressWarnings("unused")
    MessageWrapper(){}
    MessageWrapper(Payload payload, String id){
        this.type = payload.getClass().getSimpleName();
        this.id = id;
        this.payload = payload;
    }
}
