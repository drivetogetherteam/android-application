package com.drivetogether.drivetogether;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;

import com.skobbler.ngx.SKCoordinate;
import com.skobbler.ngx.SKMaps;
import com.skobbler.ngx.SKMapsInitSettings;
import com.skobbler.ngx.SKMapsInitializationListener;
import com.skobbler.ngx.map.SKAnnotation;
import com.skobbler.ngx.map.SKCoordinateRegion;
import com.skobbler.ngx.map.SKMapCustomPOI;
import com.skobbler.ngx.map.SKMapFragment;
import com.skobbler.ngx.map.SKMapPOI;
import com.skobbler.ngx.map.SKMapSettings;
import com.skobbler.ngx.map.SKMapSurfaceListener;
import com.skobbler.ngx.map.SKMapSurfaceView;
import com.skobbler.ngx.map.SKMapViewHolder;
import com.skobbler.ngx.map.SKMapViewStyle;
import com.skobbler.ngx.map.SKPOICluster;
import com.skobbler.ngx.map.SKPolyline;
import com.skobbler.ngx.map.SKScreenPoint;
import com.skobbler.ngx.map.traffic.SKTrafficIncident;
import com.skobbler.ngx.navigation.SKNavigationListener;
import com.skobbler.ngx.navigation.SKNavigationManager;
import com.skobbler.ngx.navigation.SKNavigationSettings;
import com.skobbler.ngx.navigation.SKNavigationState;
import com.skobbler.ngx.positioner.SKCurrentPositionListener;
import com.skobbler.ngx.positioner.SKCurrentPositionProvider;
import com.skobbler.ngx.positioner.SKPosition;
import com.skobbler.ngx.positioner.SKPositionerManager;
import com.skobbler.ngx.reversegeocode.SKReverseGeocoderManager;
import com.skobbler.ngx.routing.SKRouteInfo;
import com.skobbler.ngx.routing.SKRouteJsonAnswer;
import com.skobbler.ngx.routing.SKRouteListener;
import com.skobbler.ngx.routing.SKRouteManager;
import com.skobbler.ngx.routing.SKRouteSettings;
import com.skobbler.ngx.search.SKOnelineSearchSettings;
import com.skobbler.ngx.search.SKSearchListener;
import com.skobbler.ngx.search.SKSearchManager;
import com.skobbler.ngx.search.SKSearchResult;
import com.skobbler.ngx.search.SKSearchStatus;
import com.skobbler.ngx.util.SKGeoUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MapActivity extends AppCompatActivity implements SKMapsInitializationListener,
        SKMapSurfaceListener, SKRouteListener, SKNavigationListener,
        SKCurrentPositionListener, ServerListener, SKSearchListener
{
    private View mContentView;
    private SKMapSurfaceView mapView;
    private boolean isCurrentPositionSet = false;
    private SKCurrentPositionProvider currentPositionProvider;
    private SKCoordinate lastSentCoordinate;
    private ServerConnector communicationWithServer = new CommunicationWithServer(this);
    //private ServerConnector communicationWithServer = new DummyConnector(this);
    private Map<String,User> users = new HashMap<String, User>();
    private static final String TAG = "MapActivity";
    public static final int MY_PERMISSIONS_ACCESS_FINE_LOCATION=0;
    private SearchView searchFrom;
    private SearchView searchTo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_map_view);
        searchFrom = (SearchView)findViewById(R.id.searchFrom);
        searchFrom.setOnClickListener(new SearchView.OnClickListener() {
            public void onClick(View arg0) {
                SearchView a = (SearchView)arg0;
                SearchView searchTo = (SearchView)findViewById(R.id.searchTo);
                searchTo.clearFocus();
                a.setIconified(false);
            }
        });

        searchTo = (SearchView)findViewById(R.id.searchTo);
        searchTo.setOnClickListener(new SearchView.OnClickListener() {
            public void onClick(View arg0) {
                SearchView a = (SearchView)arg0;
                SearchView searchFrom = (SearchView)findViewById(R.id.searchFrom);
                searchFrom.clearFocus();
                a.setIconified(false);
            }
        });

        searchFrom.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                search();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchTo.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                search();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });



        Button fromHere = (Button) findViewById(R.id.fromHere);
        fromHere.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                SKCoordinate coordinate = SKPositionerManager.getInstance().getCurrentGPSPosition(true).getCoordinate();
                SearchView searchTo1 = (SearchView)findViewById(R.id.searchFrom);
                String name = SKReverseGeocoderManager.getInstance().reverseGeocodePosition(coordinate).getName();
                searchTo1.setQuery(name, false);
                searchTo1.setIconified(false);
                searchTo1.clearFocus();
            }
        });

        Button search = (Button) findViewById(R.id.searchB);
        search.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                search();
            }
        });
        final Button toggleNavigationButton = (Button) findViewById(R.id.goToMap);
        toggleNavigationButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                followNavigation(! mapView.getMapSettings().isFollowPositions());
            }
        });

        mContentView = findViewById(R.id.fullscreen_content);

        SKMapsInitSettings mapsInitSettings = new SKMapsInitSettings();
        mapsInitSettings.setMapResourcesPath(getExternalFilesDir(null).toString()+"/SKMaps/");
        mapsInitSettings.setPreinstalledMapsPath(getExternalFilesDir(null).toString()+"/SKMaps/PreinstalledMaps/");
        mapsInitSettings.setCurrentMapViewStyle(new SKMapViewStyle(getExternalFilesDir(null).toString() + "/SKMaps/daystyle/", "daystyle.json"));
        SKMaps.getInstance().initializeSKMaps(getApplication(), this, mapsInitSettings);

        if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED){
               setPositionOnFirstTime();
        }
        else{
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_ACCESS_FINE_LOCATION);
        }
        communicationWithServer.connect();
        hide();
    }

    private void followNavigation(boolean follow) {
        if(follow){
            mapView.getMapSettings().setHeadingMode(SKMapSettings.SKHeadingMode.ROTATING_MAP);
            mapView.getMapSettings().setFollowPositions(true);
            mapView.setZoomSmooth(16, 100);
            ((Button) findViewById(R.id.goToMap)).setText("Map");
        }else{
            mapView.getMapSettings().setHeadingMode(SKMapSettings.SKHeadingMode.NONE);
            mapView.getMapSettings().setFollowPositions(false);
            ((Button) findViewById(R.id.goToMap)).setText("Navigation");
        }

    }

    private void search(){
        SearchView searchFrom1 = (SearchView)findViewById(R.id.searchFrom);
        SearchView searchTo1 = (SearchView)findViewById(R.id.searchTo);
        String searchF = searchFrom1.getQuery().toString();
        String searchT = searchTo1.getQuery().toString();
        if(!(searchF.isEmpty()||searchT.isEmpty())){
            Log.d("DriveTogether", searchF+" "+searchT);
            try {
                startNavigation(searchT);
                Button map = (Button) findViewById(R.id.goToMap);
                map.setVisibility(View.VISIBLE);
            } catch (InterruptedException e) {
                Log.d("DriveTogether", e.getMessage());
            }
        }
    }

    private void setPositionOnFirstTime() {
        currentPositionProvider = new SKCurrentPositionProvider(this);
        currentPositionProvider.setCurrentPositionListener(this);
        currentPositionProvider.requestLocationUpdates(true, true, true);
        currentPositionProvider.requestUpdateFromLastPosition();
    }

    @Override
    public void onBackPressed() {
        finish();
    }



    @SuppressLint("InlinedApi")
    private void hide() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setPositionOnFirstTime();

                } else {
                    finish();
                }
            }
        }
    }

    @Override
    public void onLibraryInitialized(boolean b) {
        Log.d("DriveTogether", "onLibraryInitialized: " + b);
        SKMapFragment mapFragment = new SKMapFragment();
        mapFragment.setMapSurfaceListener(this);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        Log.d("DriveTogether", "onLibraryInitialized2");
        fragmentTransaction.add(R.id.fullscreen_content, mapFragment).commit();
    }

    @Override
    public void onActionPan() {

    }

    @Override
    public void onActionZoom() {

    }

    @Override
    public void onSurfaceCreated(SKMapViewHolder skMapViewHolder) {
        Log.d("DriveTogether", "onSurfaceCreated");
        mapView = skMapViewHolder.getMapSurfaceView();
        mapView.getMapSettings().setCurrentPositionShown(true);
    }

    @Override
    public void onMapRegionChanged(SKCoordinateRegion skCoordinateRegion) {

    }

    @Override
    public void onMapRegionChangeStarted(SKCoordinateRegion skCoordinateRegion) {

    }

    @Override
    public void onMapRegionChangeEnded(SKCoordinateRegion skCoordinateRegion) {

    }

    @Override
    public void onDoubleTap(SKScreenPoint skScreenPoint) {
        SKCoordinate skCoordinate = mapView.pointToCoordinate(skScreenPoint);
        searchFrom.setQuery(Double.toString(skCoordinate.getLatitude())+" "+Double.toString(skCoordinate.getLongitude()), false);
        searchFrom.setIconified(false);
        searchFrom.clearFocus();
    }

    @Override
    public void onSingleTap(SKScreenPoint skScreenPoint) {

    }

    @Override
    public void onRotateMap() {

    }

    @Override
    public void onLongPress(SKScreenPoint skScreenPoint) {
        SKCoordinate skCoordinate = mapView.pointToCoordinate(skScreenPoint);
        searchTo.setQuery(Double.toString(skCoordinate.getLatitude())+" "+Double.toString(skCoordinate.getLongitude()), false);
        searchTo.setIconified(false);
        searchTo.clearFocus();
    }

    @Override
    public void onInternetConnectionNeeded() {

    }

    @Override
    public void onMapActionDown(SKScreenPoint skScreenPoint) {

    }

    @Override
    public void onMapActionUp(SKScreenPoint skScreenPoint) {

    }

    @Override
    public void onPOIClusterSelected(SKPOICluster skpoiCluster) {

    }

    @Override
    public void onMapPOISelected(SKMapPOI skMapPOI) {

    }

    @Override
    public void onAnnotationSelected(SKAnnotation skAnnotation) {

    }

    @Override
    public void onCustomPOISelected(SKMapCustomPOI skMapCustomPOI) {

    }

    @Override
    public void onCompassSelected() {

    }

    @Override
    public void onCurrentPositionSelected() {

    }

    @Override
    public void onObjectSelected(int i) {

    }

    @Override
    public void onTrafficIncidentSelected(SKTrafficIncident skTrafficIncident) {

    }

    @Override
    public void onTrafficIncidentsClusterSelected(List<SKTrafficIncident> list) {

    }

    @Override
    public void onTrafficInfoRefreshed() {

    }

    @Override
    public void onInternationalisationCalled(int i) {

    }

    @Override
    public void onDebugInfo(double v, float v1, double v2) {

    }

    @Override
    public void onBoundingBoxImageRendered(int i) {

    }

    @Override
    public void onGLInitializationError(String s) {

    }

    @Override
    public void onScreenshotReady(Bitmap bitmap) {

    }

    public void launchRouteCalculation(SKCoordinate StartCoordinate, SKCoordinate DestinationCoordinate) {
        Log.d(TAG, "launchRouteCalculation");
        SKRouteSettings route = new SKRouteSettings();
        route.setStartCoordinate(StartCoordinate);
        route.setDestinationCoordinate(DestinationCoordinate);
        route.setMaximumReturnedRoutes(1);
        route.setRouteMode(SKRouteSettings.SKRouteMode.CAR_FASTEST);
        route.setRouteExposed(true);
        SKRouteManager.getInstance().setRouteListener(this);
        SKRouteManager.getInstance().calculateRoute(route);
    }

    @Override
    public void onRouteCalculationCompleted(final SKRouteInfo routeInfo) {
        // routeInfo contains information about the calculated route
        SKPolyline route = new SKPolyline();
        route.setNodes(SKRouteManager.getInstance().getCoordinatesForRouteByUniqueId(routeInfo.getRouteID()));
        communicationWithServer.send(route);
    }

    @Override
    public void onAllRoutesCompleted() {
        SKNavigationSettings navigationSettings = new SKNavigationSettings();
        navigationSettings.setNavigationType(SKNavigationSettings.SKNavigationType.REAL);
        navigationSettings.setShowRealGPSPositions(true);

        SKNavigationManager navigationManager = SKNavigationManager.getInstance();
        navigationManager.setMapView(mapView);
        navigationManager.setNavigationListener(this);
        navigationManager.startNavigation(navigationSettings);
        followNavigation(false);
    }

    @Override
    public void onCurrentPositionUpdate(SKPosition currentPosition) {
        if (currentPosition != null && mapView != null) {
            SKPositionerManager.getInstance().reportNewGPSPosition(currentPosition);
            if(lastSentCoordinate == null || SKGeoUtils.calculateAirDistanceBetweenCoordinates(currentPosition.getCoordinate(), lastSentCoordinate) > 5)
            {
                communicationWithServer.send(currentPosition.getCoordinate());
                lastSentCoordinate = currentPosition.getCoordinate();
            }
            if(!isCurrentPositionSet)
            {
                mapView.animateToLocation(currentPosition.getCoordinate(), 500);
                isCurrentPositionSet = true;
            }
        }
    }

    private void startNavigation(final String direction) throws InterruptedException {
        Thread thread = new Thread() {
            public void run() {
                try {
                    while(!isCurrentPositionSet) {
                        TimeUnit.SECONDS.sleep(1);
                    }

                    runSearch(direction, SKPositionerManager.getInstance().getCurrentGPSPosition(true).getCoordinate());
                } catch(InterruptedException v) {
                    System.out.println(v);
                }
            }
        };
        thread.start();
    }

    private void startNavigation(SKCoordinate yourCoordinate, SKCoordinate directionCoordinate){
        mapView.animateToLocation(yourCoordinate, 0);
        launchRouteCalculation(yourCoordinate,directionCoordinate);
    }

    @Override
    public void setUserPosition(String userID, SKCoordinate coordinate){
        if(!users.containsKey(userID)){
            users.put(userID, new User(Integer.decode("0x" + userID), mapView));
        }
        users.get(userID).setPosition(coordinate);
    }

    @Override
    public void setUserRoute(String userID, SKPolyline route){
        if(!users.containsKey(userID)){
            users.put(userID, new User(Integer.decode("0x"+userID), mapView));
        }
        users.get(userID).setRoute(route);
    }

    @Override
    public void removeUser(String userID){
        if(!users.containsKey(userID)){
            users.get(userID).removeUser();
        }
    }

    @Override
    public void onOnlineRouteComputationHanging(int status) { }

    @Override
    public void onServerLikeRouteCalculationCompleted(SKRouteJsonAnswer status) { }

    @Override
    public void onRouteCalculationFailed(SKRouteListener.SKRoutingErrorCode statusCode){ }

    @Override
    public void onFreeDriveUpdated(String countryCode, String streetName, String x, SKNavigationState.SKStreetType streetType, double currentSpeed, double speedLimit) { }

    @Override
    public void onSpeedExceededWithInstruction(String adviceList, boolean speedExceeded) { }

    @Override
    public void onReRoutingStarted() { }

    @Override
    public void onFcdTripStarted(String s) { }

    @Override
    public void onTunnelEvent(boolean tunnelEntered) { }

    @Override
    public void onVisualAdviceChanged(boolean firstVisualAdviceChanged, boolean secondVisualAdviceChanged, SKNavigationState navigationState) { }

    @Override
    public void onViaPointReached(int n) { }

    @Override
    public void onDestinationReached() { }

    @Override
    public void onSpeedExceededWithAudioFiles(String[] adviceList, boolean speedExceeded) { }

    @Override
    public void onSignalNewAdviceWithInstruction(String audioFiles) { }

    @Override
    public void onUpdateNavigationState(SKNavigationState navigationState) {}

    @Override
    public void onSignalNewAdviceWithAudioFiles(String[] audioFiles, boolean specialSoundFile) { }

    public void runSearch(String searchTerm, SKCoordinate pos) {
        SKSearchManager searchManager = new SKSearchManager(this);
        SKOnelineSearchSettings onelineSearchSettings = new SKOnelineSearchSettings(searchTerm, SKSearchManager.SKSearchMode.ONLINE);
        // Set the position around which to do the search
        onelineSearchSettings.setGpsCoordinates(pos);
        onelineSearchSettings.setOnlineGeocoder(SKOnelineSearchSettings.SKGeocoderType.MAP_SEARCH_GOOGLE);
        // Set the maximum number of results
        onelineSearchSettings.setSearchResultsNumber(1);
        // The status value indicates if the search could be performed
        SKSearchStatus searchStatus = searchManager.onelineSearch(onelineSearchSettings);
    }

    @Override
    public void onReceivedSearchResults(List<SKSearchResult> list) {
        startNavigation(SKPositionerManager.getInstance().getCurrentGPSPosition(true).getCoordinate(), list.get(0).getLocation());
    }

    // Call Back method  to get the Message form other Activity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==2)
        {
            //do the things u wanted
        }
    }
}