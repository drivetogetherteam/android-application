package com.drivetogether.drivetogether;

import com.skobbler.ngx.SKCoordinate;
import com.skobbler.ngx.map.SKPolyline;

/**
 * Created by gumny on 28.01.17.
 */

public class DummyConnector implements ServerConnector {

    private ServerListener listener;

    public DummyConnector(ServerListener listener){
        this.listener = listener;
    }

    @Override
    public void connect() {

    }

    @Override
    public void send(String text) {

    }

    @Override
    public <Payload> void send(Payload payload) {
        switch (payload.getClass().getSimpleName()){
            case "SKCoordinate":
                listener.setUserPosition("0", (SKCoordinate) payload);
                break;
            case "SKPolyline":
                listener.setUserRoute("0", (SKPolyline) payload);
                break;
        }
    }
}
